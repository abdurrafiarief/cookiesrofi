certifi==2019.6.16
chardet==3.0.4
dj-database-url==0.5.0
Django==2.2.1
heroku==0.1.4
idna==2.8
python-dateutil==1.5
pytz==2019.2
requests==2.22.0
urllib3==1.25.3
whitenoise==4.1.3
wincertstore==0.2
gunicorn==19.9.0
selenium == 3.141.0
coverage == 4.5.1