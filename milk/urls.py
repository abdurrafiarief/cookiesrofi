from django.urls import path, re_path
from .views import *

app_name = "milk"

urlpatterns = [
    path('', landing, name="landing"),
    re_path(r'^signup/$', signup_view, name = "signup"),
    path('accounts/profile/', profile, name="profile")
]