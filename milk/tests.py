from django.test import TestCase, Client, LiveServerTestCase
import time

from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
# Create your tests here.

class Cookies_Test(TestCase):
    def test_cookies_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "cookies.html")

    def test_signup_url(self):
        c = Client()
        response = c.get('/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "signup.html")

    def test_login_url(self):
        c = Client()
        response = c.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
    
    def test_signup(self):
        count = User.objects.all().count()
        self.assertEqual(count, 0)

        data = {
            'username':'kucing',
            'password1': 'kucinganjing1',
            'password2': 'kucinganjing1',
        }

        response = self.client.post("/signup/", data)
        count2 = User.objects.all().count()

        self.assertEqual(count2, 1)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "cookies.html")

    def test_session_id_exist_when_logged_in(self):
        user = User.objects.create_user('myusername', 'myemail@crazymail.com', 'mypassword')
        user.first_name = 'Putra'
        user.save()
        response = self.client.post('/accounts/login/', data={'username':'myusername', 'password':'mypassword'})
        self.assertEqual(response.status_code, 302)
        self.assertIn('_auth_user_id', self.client.session)

    

