from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import user_logged_in
# Create your views here.
def landing(request):
    return render(request, "cookies.html")

def signup_view(request):
    if request.method == 'POST':
        signupForm = UserCreationForm(request.POST)
        if signupForm.is_valid():
            signupForm.save()
        return render(request, "cookies.html")
    else:
        signupForm = UserCreationForm()
    return render(request, 'signup.html', {'form': signupForm})


def profile(request):
    return render(request, 'registration/profile.html')